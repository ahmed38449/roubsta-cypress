// register as a parent
// import the Object_identifiers file 
import {registration_page} from "../../Object_identifiers/registration_page" 


/// <reference types="cypress" />

const registration = new registration_page()

describe('Login to OrangeHRM website', function () {
  before(function () {
      cy.fixture('testdata').then(function (testdata) {
          this.testdata = testdata
        })
      })      
 
     it('validateRegisterAsParent', function () {

      registration.navigateHomepage() //visit a tagret url using Object_identifiers

      cy.get('.css-3pqtzz').contains('تسجيل حساب').click() 

      cy.wait(4500) // wait 4 seconds after page load

      // click on selected element 
      cy.get('#roles > :nth-child(1) > .css-ubxa2z').click() 

      // typing fname, lastname and mob number from useing fixtures
      cy.get('[data-cy=firstName-input]').type(this.testdata.fname)

      cy.get('[data-cy=lastName-input]').type(this.testdata.lname)

      cy.get('[data-cy=mobileNumber-input]').type(this.testdata.mobileNo)   


      cy.get('#gender > :nth-child(1) > .css-ubxa2z').click()
      cy.get('[data-cy=ageRange-input]').select(['من 25 الي 35 سنة']) // select from dropdwon menu
      
      // typing the passowrd from useing fixtures

      cy.get('[data-cy=password-input]').type(this.testdata.password) 

      cy.get('[data-cy=passwordConfirmation-input]').type(this.testdata.repassword) 


      // using assertions to select and check the element
      cy.get('#termsAndConditionsCheck')
       .should('not.be.visible')
       .check({ force: true }).should('be.checked')

      // ignoring the google Recaptcha
      // must have recaptcha configured for test mode run the below code: https://developers.google.com/recaptcha/docs/faq#id-like-to-run-automated-tests-with-recaptcha.-what-should-i-do
       Cypress.Commands.add("clickRecaptcha", () => {
            cy.window().then(win => {
              win.document
                .querySelector("iframe[src*='recaptcha']")
                .contentDocument.getElementById("recaptcha-token")
                .click();
            })
          
      // submit the registration form
            cy.get('[data-cy=submitButton]').click()



            

  
   


     
    })
})
})
