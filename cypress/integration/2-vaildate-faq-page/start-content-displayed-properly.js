// validate the faq page contents
// import the Object_identifiers file 
import {registration_page} from "../../Object_identifiers/registration_page" 

/// <reference types="cypress" />

let faqPage = new registration_page ()

it('Actions', () => {

    faqPage.navigateFaq() // visit a tagret url using Object_identifiers
  
      // fist container of questions
      // using assertions to select the element and validate the css
      cy.get(':nth-child(1) > .Collapsible__trigger > .css-olkrgj').contains('ما هي المؤسسة ؟').should('have.css', 'font-size', '17px').click()
      cy.contains('مؤسسة ڤودافون مصر لتنمية المجتمع هي أول مؤسسة لا تهدف للربح في مجال الاتصالات في مصر وهي تابعة لوزارة التضامن الاجتماعي وتختلف عن قسم المسؤولية المجتمعية الخاص بالشركة. من وقت تأسيسها في سنة 2003 والمؤسسة بتحاول جاهدة إنها تنمي المجتمع من خلال إشراك منظمات المجتمع المدني والهيئات الغير حكومية في عملية تطوير المجتمع المصري في مجالات التعليم وتمكين ذوي الاحتياجات الخاصة.').should('have.css', 'font-size', '14px')
      cy.get(':nth-child(1) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(1) > .Collapsible__trigger > .css-olkrgj > .arrow').click()

      cy.get(':nth-child(2) > .Collapsible__trigger > .css-olkrgj').contains('ما هو تعليمي ؟').should('have.css', 'font-size', '17px').click()
      cy.contains('تعليمي هي منصة إلكترونية تعليمية مجانية للطلاب وأولياء الأمور والمعلمين').should('have.css', 'font-size', '14px')
      cy.get(':nth-child(2) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(2) > .Collapsible__trigger > .css-olkrgj > .arrow').click()

      cy.get(':nth-child(3) > .Collapsible__trigger > .css-olkrgj').contains('من المستفيد من تعليمي ؟').should('have.css', 'font-size', '17px').click()
      cy.contains('أولياء الأمور والطلاب في المرحلة الأولي وستستهدف المرحلة القادمة تقديم محتوى للمعلمين').should('have.css', 'font-size', '14px')
      cy.get(':nth-child(3) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(3) > .Collapsible__trigger > .css-olkrgj > .arrow').click()

      
      cy.get(':nth-child(4) > .Collapsible__trigger > .css-olkrgj').contains('ما هو المحتوي الموجود في تعليمي؟').should('have.css', 'font-size', '17px').click()
      cy.contains('لأولياء الأمور يوجد:').should('have.css', 'font-size', '14px')
      cy.contains('المعلومات عن التربية الإيجابية').should('have.css', 'font-size', '14px')
      cy.contains('نصائح لتعليم الأولاد استخدام التكنولوجيا بطريقة آمنة').should('have.css', 'font-size', '14px')
      cy.contains('خلال كتب إلكترونية باللغة العربية').should('have.css', 'font-size', '14px')
      cy.contains('اختبار لاكتشاف قدرات الأولادك').should('have.css', 'font-size', '14px')
      cy.contains('للطلاب يوجد:').should('have.css', 'font-size', '14px')
      cy.contains('بالشراكة مع الأضواء يوجد مناهج لكل المراحل الدراسية').should('have.css', 'font-size', '14px')
      cy.get(':nth-child(4) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(4) > .Collapsible__trigger > .css-olkrgj > .arrow').click()
      

      cy.get(':nth-child(5) > .Collapsible__trigger > .css-olkrgj').contains('هل تعليمي مجاني ؟').should('have.css', 'font-size', '17px').click()
      cy.contains('تعليمي مجاني لجميع الأفراد بشكل عام ويتميز عملاء ڤودافون بعدم السحب من باقة الانترنت بشكل خاص').should('have.css', 'font-size', '14px')
      cy.get(':nth-child(5) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(5) > .Collapsible__trigger > .css-olkrgj > .arrow').click()




      cy.get(':nth-child(6) > .Collapsible__trigger > .css-olkrgj').contains('كيف أقوم باستخدام تعليمي ؟').should('have.css', 'font-size', '17px').click()
      cy.get(':nth-child(6) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(6) > .Collapsible__trigger > .css-olkrgj > .arrow').click()





      cy.get(':nth-child(7) > .Collapsible__trigger > .css-olkrgj').contains('كيف احصل على النقاط في لوحة المتصدرين ').should('have.css', 'font-size', '17px').click()
      cy.get(':nth-child(7) > .Collapsible__trigger > .css-olkrgj').click()
      cy.get(':nth-child(7) > .Collapsible__trigger > .css-olkrgj > .arrow').click()



    })